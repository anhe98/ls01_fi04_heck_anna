import java.util.Scanner;

public class Schleifenübung {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 0;
      double y;
      double m;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Wie viele Zahlen möchten sie eingeben ");
      int anzahl = scan.nextInt();
      
      
      for (int i = 1; i <= anzahl; i++) {
    	  System.out.println("Bitte geben sie ihre zahl ein: ");
    	  x += scan.nextDouble();
      }
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}