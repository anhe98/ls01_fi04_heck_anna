package egal;

public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf( "%11s |", "Fahrenheit" );
		System.out.printf( "%10s \n", "Celsius" );
		System.out.println("------------------------");
		
		System.out.printf("-20%10s", "|");
		System.out.printf("%10s\n", "-28.89");
		System.out.printf("-10%10s", "|");
		System.out.printf("%10s\n", "-23.33");
		System.out.printf("+0%11s", "|");
		System.out.printf("%10s\n", "-17.78");
		System.out.printf("+20%10s", "|");
		System.out.printf("%10s\n", "-6.67");
		System.out.printf("+30%10s", "|");
		System.out.printf("%10s\n", "-1.11");
		
	}
	
	
}
