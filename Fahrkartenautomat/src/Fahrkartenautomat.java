﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		int anzahlTicket;
		int ticketArt;

		do {
			zuZahlenderBetrag = farkartenBestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			System.out.println("Neuer Kaufvorgang:");
			System.out.println(" ");
		} while (true);
	}

	public static double farkartenBestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        
        /*zu Aufgabe 1: Durch die Verwendung von Arrays kann man die Ausgabe mithilfe einer Schleife mit weniger Code realisieren.
         * Außerdem ist es so möglich eine weitere Ticket Art und einen Preis hinzuzufügen ohne den komplettenm Code ändern zu müssen.
        */
        
        double ticketPreis = 0.0;
        
        double[] ticketPreise = {1.90,2.90,3.30,3.60,8.60,9.00,9.60,23.50,24.30,24.90};
        
        
        String[] ticketArt = {"Kurzstrecke [1,90 EUR] (1)","Einzelfahrschein Berlin AB [2,90 EUR] (2)","Einzelfahrschein Berlin BC [3,30 EUR] (3)",
        						"Einzelfahrschein Berlin ABC [3,60 EUR] (4)", "Tageskarte AB [8,60 EUR] (5)","Tageskarte BC [9,00 EUR] (6)",
        						"Tageskarte ABC [9,60 EUR] (7)","Kleingrupen-Tageskarte AB [23,50 EUR] (8)",
        						"Kleingrupen-Tageskarte BC [24,30 EUR] (9)","Kleingrupen-Tageskarte ABC [24,90 EUR] (10)"};
  
        double zuZahlenderBetrag;
        
        
        
        for(int i=0;i<ticketArt.length;i++){
            System.out.println(ticketArt[i]);
        }
               
        
        System.out.println(" ");
        System.out.println("Welches Ticket möchtest du auswählen?");
        int welchesTicket = tastatur.nextInt();  
        
        
        if (welchesTicket > ticketArt.length || welchesTicket < 1)
        {
         welchesTicket = 1;
         System.out.println("Eingabe ungültig. Sie kaufen nun eine Kurzstrecke.");
        }
               
        ticketPreis = ticketPreise[welchesTicket-1];  
        System.out.println("Du zahlst " + ticketPreis + "0 €.");
        
        //Anzahl der Tickets
        System.out.print("Anzahl der Tickets: ");
        int anzahlTicket = tastatur.nextInt();
        
        //Nur höchstens 10 Tickets angeben:
        	
        if (anzahlTicket > 10 || anzahlTicket < 1)
        {
         anzahlTicket = 1;
         System.out.println("Nur Eingabe zwischen 1 und 10 gültig. Anzahl Ticket wurde auf 1 gesetzt");
        }
 

        zuZahlenderBetrag = ticketPreis * anzahlTicket;
        return zuZahlenderBetrag;
    }

	// Fahrkarten bezahlen
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehendeBetrage = zuZahlenderBetrag;
			ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}
